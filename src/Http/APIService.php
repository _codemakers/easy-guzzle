<?php

namespace StephenBrown90\EasyGuzzle\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

abstract class APIService {

	protected $guzzle;
	protected $endpoint;
	protected $requestHeaders;
	protected $apiResponse;

	/**
	 * Constructor for APIService - Base class to allow API calls to be made
	 *
	 * @param      Client  $guzzle  Guzzle Client
	 *
	 * @author     Stephen
	 */
	public function __construct(Client $guzzle, APIResponse $apiResponse) {

		$this->guzzle = $guzzle;
		$this->apiResponse = $apiResponse;
		$this->requestHeaders = array('content-type' => 'application/json');
	}

	/**
	 * Send an HTTP request to the API
	 *
	 * @param      string       $type   HTTP Method
	 * @param      string       $path   Path to endpoint
	 * @param      array        $body   Request body
	 * @param      array        $auth   The auth
	 *
	 * @throws     \Exception   (description)
	 *
	 * @return     APIResponse
	 *
	 * @author     Stephen
	 */
	protected function sendRequest($type, $path, $body = array(), $auth = [])
	{	
		// log the request that is beign attempted	
		$this->logRequest($type, $path, $body);

		try {
			// build the data array including headers and the body
			$data = [
				'body' => json_encode($body),
				'headers' => $this->requestHeaders
			];

			if(!empty($auth)) {
				$data["auth"] = $auth;
			}

			// make the API call
			$response = $this->guzzle->{$type}(
				$this->endpoint . $path, 
				$data
			);
			
			// get the response type - handle different file types
			$this->handleContentType($response);
			

			// get the body of the response
			$body = json_decode($response->getBody());

			// Ensure we successfully decoded the response
			if(json_last_error() !== JSON_ERROR_NONE) {
				throw new \Exception("Failed to decode JSON response: " . json_last_error());
			}

			// create a new APIResponse
			$returnResponse = new APIResponse;
			return $this->handleResponse($returnResponse, $body, $response);

		
		} catch(RequestException $e) {
			// we have had a bad response
			$response = $e->getResponse();
			$body = json_decode($response->getBody());

			$returnResponse = new APIResponse;
			return $this->handleResponse($returnResponse, $body, $response);
		}
	}

	protected function logRequest($type, $path, $body) {
		// do nothing -- override if required
	}

	protected function logResponse($response) {
		// do nothing -- override if required
	}

	// override this for custom content type handling
	protected function handleContentType($response) {
		// do nothing -- override if required
	}

	// override this for custom response handle
	protected function handleResponse(APIResponse $returnResponse, \StdClass $body, $response): APIResponse {
		$returnResponse->setHttpCode($response->getStatusCode());
		$returnResponse->setData(isset($body->data) ? $body->data : null);
		$returnResponse->setMessage(isset($body->message) ? $body->message : null);

		$this->logResponse($returnResponse);
		
		return $returnResponse;
	}
}