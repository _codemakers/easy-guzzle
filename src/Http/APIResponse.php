<?php

namespace StephenBrown90\EasyGuzzle\Http;

class APIResponse {
	protected $httpCode;
	protected $data;
	protected $message;

	public function setHttpCode($httpCode) {
		$this->httpCode = $httpCode;
	}

	public function httpCode() {
		return $this->httpCode;
	}

	public function setData($data) {
		$this->data = $data;
	}

	public function data() {
		return $this->data;
	}

	public function setMessage($message) {
		$this->message = $message;
	}

	public function message() {
		return $this->message;
	}

	public function reset() {
		$this->SetMessage(null);
		$this->setHttpCode(null);
		$this->setData(null);
	}
}