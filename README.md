# Easy Guzzle
## Example Usage
```
<?php
require_once 'vendor/autoload.php';

use StephenBrown90\EasyGuzzle\Http\APIService;
use StephenBrown90\EasyGuzzle\Http\APIResponse;
use GuzzleHttp\Client;

class MyService extends APIService {
	public function __construct() {
		parent::__construct((new GuzzleHttp\Client), (new APIResponse));

		$this->endpoint = "https://api.shipmate.co.uk/v1/";
	}

	public function send_test() {
		$response = $this->sendRequest('POST', 'consignments', ['data' => 'value']);
		var_dump($response);
	}

	// override
	protected function logResponse($response) {
		
	}

	// override
	protected function logRequest($type, $path, $body) {

	}

	// override
	protected function handleContentType($response) {
		// do nothing -- override if required
		$contentType = $response->getHeader('Content-Type');
		// do nothing right now>
		if(isset($contentType[0]) && $contentType[0] == "application/pdf") {
			// we have been returned a PDF - let us output it 
			header( 'Content-Description: File Transfer');
			header( 'Content-Type: application/pdf');
			header( 'Content-Disposition: attachment; filename=label.pdf');
			
			echo $response->getBody();
			exit();
		}
	}
}

$myService = new MyService();
$myService->send_test();
?>
```